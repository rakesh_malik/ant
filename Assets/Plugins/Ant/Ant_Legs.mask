%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Ant_Legs
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: ant
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/Base
    m_Weight: 1
  - m_Path: Armature/Base/Back
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FL_Thigh
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FL_Thigh/Leg_FL_Step1
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FL_Thigh/Leg_FL_Step1/Leg_FL_Step2
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FL_Thigh/Leg_FL_Step1/Leg_FL_Step2/Leg_FL_Step3
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FL_Thigh/Leg_FL_Step1/Leg_FL_Step2/Leg_FL_Step3/Leg_FL_Step4
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FR_Step0
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FR_Step0/Leg_FR_Step1
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FR_Step0/Leg_FR_Step1/Leg_FR_Step2
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FR_Step0/Leg_FR_Step1/Leg_FR_Step2/Leg_FR_Step3
    m_Weight: 1
  - m_Path: Armature/Base/Back/Leg_FR_Step0/Leg_FR_Step1/Leg_FR_Step2/Leg_FR_Step3/Leg_FR_Step4
    m_Weight: 1
  - m_Path: Armature/Base/Back/Neck1
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_L_Step1
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_L_Step1/Antenna_L_Step2
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_L_Step1/Antenna_L_Step2/Antenna_L_Step3
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_L_Step1/Antenna_L_Step2/Antenna_L_Step3/Antenna_L_Step4
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_L_Step1/Antenna_L_Step2/Antenna_L_Step3/Antenna_L_Step4/Antenna_L_Step5
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_R_Step1
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_R_Step1/Antenna_R_Step2
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_R_Step1/Antenna_R_Step2/Antenna_R_Step3
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_R_Step1/Antenna_R_Step2/Antenna_R_Step3/Antenna_R_Step4
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Antenna_R_Step1/Antenna_R_Step2/Antenna_R_Step3/Antenna_R_Step4/Antenna_R_Step5
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Head
    m_Weight: 0
  - m_Path: Armature/Base/Back/Neck1/Neck2/Head/Jaw
    m_Weight: 0
  - m_Path: Armature/Base/Leg_BL_Joint
    m_Weight: 1
  - m_Path: Armature/Base/Leg_BL_Joint/Leg_BL_Step0
    m_Weight: 1
  - m_Path: Armature/Base/Leg_BL_Joint/Leg_BL_Step0/Leg_BL_Step1
    m_Weight: 1
  - m_Path: Armature/Base/Leg_BL_Joint/Leg_BL_Step0/Leg_BL_Step1/Leg_BL_Step2
    m_Weight: 1
  - m_Path: Armature/Base/Leg_BL_Joint/Leg_BL_Step0/Leg_BL_Step1/Leg_BL_Step2/Leg_BL_Step3
    m_Weight: 1
  - m_Path: Armature/Base/Leg_BR_Joint
    m_Weight: 1
  - m_Path: Armature/Base/Leg_BR_Joint/Leg_BR_Step0
    m_Weight: 1
  - m_Path: Armature/Base/Leg_BR_Joint/Leg_BR_Step0/Leg_BR_Step1
    m_Weight: 1
  - m_Path: Armature/Base/Leg_BR_Joint/Leg_BR_Step0/Leg_BR_Step1/Leg_BR_Step2
    m_Weight: 1
  - m_Path: Armature/Base/Leg_BR_Joint/Leg_BR_Step0/Leg_BR_Step1/Leg_BR_Step2/Leg_BR_Step3
    m_Weight: 1
  - m_Path: Armature/Base/Leg_ML_Joint
    m_Weight: 1
  - m_Path: Armature/Base/Leg_ML_Joint/Leg_ML_Step0
    m_Weight: 1
  - m_Path: Armature/Base/Leg_ML_Joint/Leg_ML_Step0/Leg_ML_Step1
    m_Weight: 1
  - m_Path: Armature/Base/Leg_ML_Joint/Leg_ML_Step0/Leg_ML_Step1/Leg_ML_Step2
    m_Weight: 1
  - m_Path: Armature/Base/Leg_ML_Joint/Leg_ML_Step0/Leg_ML_Step1/Leg_ML_Step2/Leg_ML_Step3
    m_Weight: 1
  - m_Path: Armature/Base/Leg_MR_Joint
    m_Weight: 1
  - m_Path: Armature/Base/Leg_MR_Joint/Leg_MR_Step0
    m_Weight: 1
  - m_Path: Armature/Base/Leg_MR_Joint/Leg_MR_Step0/Leg_MR_Step1
    m_Weight: 1
  - m_Path: Armature/Base/Leg_MR_Joint/Leg_MR_Step0/Leg_MR_Step1/Leg_MR_Step2
    m_Weight: 1
  - m_Path: Armature/Base/Leg_MR_Joint/Leg_MR_Step0/Leg_MR_Step1/Leg_MR_Step2/Leg_MR_Step3
    m_Weight: 1
  - m_Path: Armature/Base/Weist
    m_Weight: 0
  - m_Path: Armature/Base/Weist/Hip
    m_Weight: 0
