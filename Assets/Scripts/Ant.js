﻿#pragma strict
 
/* <summary>
 * from http://answers.unity3d.com/questions/155907/basic-movement-walking-on-walls.html
 * </summary>
 */
 
 var isAI: boolean = false;
 private var seed1: float;
 private var seed2: float;
 
 var moveSpeed: float = 6; // move speed
 var turnSpeed: float = 90; // turning speed (degrees/second)
 var lerpSpeed: float = 10; // smoothing speed
 var gravity: float = 10; // gravity acceleration
 var isGrounded: boolean;
 var deltaGround: float = 0.2; // character is grounded up to this distance
 var jumpSpeed: float = 10; // vertical jump initial speed
 var jumpRange: float = 10; // range to detect target wall
 
 var sprintKey: KeyCode = KeyCode.LeftShift;
 var sprintMultiplier: float = 2;
 var acceleration: float = 0.1;
 
 private var surfaceNormal: Vector3; // current surface normal
 private var myNormal: Vector3; // character normal
 private var distGround: float; // distance from character position to ground
 private var jumping = false; // flag &quot;I'm jumping to wall&quot;
 private var vertSpeed: float = 0; // vertical jump current speed 
 
 private var animator: Animator = null;
 private var aiDisabled = false;
 
 private var targetSpeed: float = 0;
 private var currentSpeed: float = 0;
 
 function Start(){
     myNormal = transform.up; // normal starts as character up direction 
     GetComponent.<Rigidbody>().freezeRotation = true; // disable physics rotation
     // distance from transform.position to ground
     distGround = GetComponent.<BoxCollider>().bounds.extents.y - GetComponent.<BoxCollider>().center.y;
     seed1 = Random.RandomRange(0f, 1f);
     seed2 = Random.RandomRange(0f, 1f);
     
     animator = transform.FindChild("ant").GetComponent.<Animator>();
     
     if(isAI) {
     	ControlPhysics();
     }
 }
 
 function FixedUpdate(){
 	 if(aiDisabled) return;
 	 
     // apply constant weight force according to character normal:
     GetComponent.<Rigidbody>().AddForce(-gravity*GetComponent.<Rigidbody>().mass*myNormal);
 }
 
 function Update(){
 	 if(aiDisabled) return;
 
     // jump code - jump to wall or simple jump
     if (jumping) return;  // abort Update while jumping to a wall
     var ray: Ray;
     var hit: RaycastHit;
     if (getInput("Jump")){ // jump pressed:
         ray = Ray(transform.position, transform.forward);
         if (Physics.Raycast(ray, hit, jumpRange)){ // wall ahead?
             JumpToWall(hit.point, hit.normal); // yes: jump to the wall
         }
         else if (isGrounded){ // no: if grounded, jump up
             GetComponent.<Rigidbody>().velocity += jumpSpeed * myNormal;
         }                
     }
     
     // movement code - turn left/right with Horizontal axis:
     transform.Rotate(0, getInput("Horizontal")*turnSpeed*Time.deltaTime, 0);
     // update surface normal and isGrounded:
     ray = Ray(transform.position, -myNormal); // cast ray downwards
     if (Physics.Raycast(ray, hit)){ // use it to update myNormal and isGrounded
         isGrounded = hit.distance <= distGround + deltaGround;
         surfaceNormal = hit.normal;
     }
     else {
         isGrounded = false;
         // assume usual ground normal to avoid "falling forever"
         surfaceNormal = Vector3.up; 
     }
     myNormal = Vector3.Lerp(myNormal, surfaceNormal, lerpSpeed*Time.deltaTime);
     // find forward direction with new myNormal:
     var myForward = Vector3.Cross(transform.right, myNormal);
     // align character to the new myNormal while keeping the forward direction:
     var targetRot = Quaternion.LookRotation(myForward, myNormal);
     transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, lerpSpeed*Time.deltaTime);
     // move the character forth/back with Vertical axis:
     var inputVertical: float = getInput("Vertical");
     transform.Translate(0, 0, inputVertical*moveSpeed*Time.deltaTime); 
     
     animator.SetFloat("velocity", inputVertical);
 }
 
 function JumpToWall(point: Vector3, normal: Vector3){
     // jump to wall 
     jumping = true; // signal it's jumping to wall
     GetComponent.<Rigidbody>().isKinematic = true; // disable physics while jumping
     var orgPos = transform.position;
     var orgRot = transform.rotation;
     var dstPos = point + normal * (distGround + 0.5); // will jump to 0.5 above wall
     var myForward = Vector3.Cross(transform.right, normal);
     var dstRot = Quaternion.LookRotation(myForward, normal);
     for (var t: float = 0.0; t < 1.0; ){
         t += Time.deltaTime;
         transform.position = Vector3.Lerp(orgPos, dstPos, t);
         transform.rotation = Quaternion.Slerp(orgRot, dstRot, t);
         yield; // return here next frame
     }
     myNormal = normal; // update myNormal
     GetComponent.<Rigidbody>().isKinematic = false; // enable physics
     jumping = false; // jumping to wall finished
 }
 
 function getInput(input) {
 	if(isAI) {
 		switch(input) {
 			case "Jump":
 				return false;
 				break;
 			case "Vertical":
 				return Mathf.PerlinNoise(gameObject.GetHashCode(), Time.fixedTime * seed1);
 				break;
 			case "Horizontal":
 				return Mathf.PerlinNoise(gameObject.GetHashCode(), Time.fixedTime * seed2) - 0.5;
 				break;
 		}
 	} else {
 		switch(input) {
 			case "Jump":
 				return Input.GetButtonDown(input);
 				break;
 			case "Vertical":
 				if(Input.GetKey(sprintKey)) {
 					targetSpeed = Input.GetAxis(input) * sprintMultiplier;
 				} else {
 					targetSpeed = Input.GetAxis(input);
 				}
 				currentSpeed = Mathf.Lerp(currentSpeed, targetSpeed, acceleration);
 				return currentSpeed;
 				break;
 			case "Horizontal":
 				return Input.GetAxis(input);
 				break;
 		}
 	}
 }
 
 function ControlPhysics() {
 	while(true) {
 		yield WaitForSeconds(0.25);
 		
 		aiDisabled = (Camera.current != null && Vector3.Distance(transform.position, Camera.current.transform.position) > 100);
 		if(aiDisabled) {
 			GetComponent.<Rigidbody>().Sleep();
 			animator.SetFloat("velocity", 0f);
 		}
 	}
 }
 