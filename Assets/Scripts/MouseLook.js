﻿
/// MouseLook rotates the transform based on the mouse delta.
/// Minimum and Maximum values can be used to constrain the possible rotation

/// To make an FPS style character:
/// - Create a capsule.
/// - Add the MouseLook script to the capsule.
///   -> Set the mouse look to use LookX. (You want to only turn character but not tilt it)
/// - Add FPSInputController script to the capsule
///   -> A CharacterMotor and a CharacterController component will be automatically added.

/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add a MouseLook script to the camera.
///   -> Set the mouse look to use LookY. (You want the camera to tilt up and down like a head. The character already turns.)

public enum MouseRotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	
public class MouseLook extends MonoBehaviour {

	public var axes: MouseRotationAxes = MouseRotationAxes.MouseXAndY;
	public var sensitivityX: float = 15F;
	public var sensitivityY: float = 15F;

	public var minimumX: float = -360F;
	public var maximumX: float= 360F;

	public var minimumY: float = -60F;
	public var maximumY: float = 60F;

	var rotationY: float = 0F;

	function Update ()
	{
		var previousAngle: Quaternion = transform.rotation;
		if (axes == MouseRotationAxes.MouseXAndY)
		{
			var rotationX: float = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX * Time.timeScale;
			if(rotationX > 180) rotationX = rotationX - 360;
			rotationX = Mathf.Clamp (rotationX, minimumX, maximumX);
			
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY * Time.timeScale;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
		}
		else if (axes == MouseRotationAxes.MouseX)
		{
			transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX * Time.timeScale, 0);
		}
		else
		{
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY * Time.timeScale;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
		}
		transform.rotation = Quaternion.Lerp (previousAngle, transform.rotation, 0.333f);
	}
	
	function Start ()
	{
		// Make the rigid body not change rotation
		if (GetComponent.<Rigidbody>())
			GetComponent.<Rigidbody>().freezeRotation = true;
	}
	
	function OnEnable() {
		Start();
	}
}